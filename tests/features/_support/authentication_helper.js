module.exports = function () {
  this.Before(function () {

    this.AuthenticationHelper = {
      login: function () {
        client.waitForExist('a#loginlink');
        client.click('a#loginlink');
        client.setValue('#at-field-username_and_email', 'testuser');
        client.setValue('#at-field-password', 'test123');
        client.click('#at-btn');
      },

      logout: function () {
        client.executeAsync(function (done) {
          Meteor.logout(done);
        });
      },

      createAccount: function () {
        return server.call('createTestAccount');
      },

      createAccountAndLogin : function(profile) {
        this.createAccount(profile);
        this.login();
      }
    };

  });
};
