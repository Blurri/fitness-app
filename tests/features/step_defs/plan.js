module.exports = function  () {


  this.Given(/^I am logged in and on new plan page$/, function (callback) {
    browser.url(process.env.ROOT_URL);
    this.AuthenticationHelper.login();
    client.waitForExist('a#newplanlink');
    client.click('a#newplanlink');
    callback()
	});

  this.When(/^I add a plan$/, function (callback) {
    // Write code here that turns the phrase above into concrete actions
    client.waitForExist('#plantitle');
    client.setValue('#plantitle', "My First Plan");
    client.click("button#addplan")
    callback();
  });

  this.Then(/^I see the Plan Title in the updateinput$/, function () {
    client.waitForExist('#updateinput');
    var text = client.getValue('#updateinput');
    console.log(text);
  });

}