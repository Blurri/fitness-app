StatusimagesList = React.createClass({
  mixins: [EnsureAuthenticated, ReactMeteorData],
  getMeteorData() {
    let data = {};
    let handle = Meteor.subscribe('allStatusimages');
    if(handle.ready()){
      data.statusimages = Statusimages.find().fetch();
    }
    return data;
  },
  showDetailView(_id) {
    FlowRouter.go('/statusimages/'+_id);
  },
  getContent() {
    return (
      <div className="statusimages-list">
        <div className="card-columns">
          {this.data.statusimages.map((entry,index)=>{
             return (
             <div className="card" key={index} >

              <h6 className="card-title">{moment(entry.createdAt).fromNow()}</h6>
              {entry.images ?
                <img className="card-img-top img-fluid" src={entry.images[0].url} alt="Card image cap" />
                :
                null
              }
              <div className="card-block">
                <p className="card-text">
                  {entry.comment}
                </p>
                <button className="btn btn-info btn-sm" onClick={this.showDetailView.bind(this,entry._id)}>Detail</button>
              </div>
            </div>)
          })}
        </div>
      </div>
    )
  },
  render() {
    return (
      <div>
        {this.data.statusimages ? this.getContent() : <p>Loading ...</p>}
      </div>
    )
  }
})
