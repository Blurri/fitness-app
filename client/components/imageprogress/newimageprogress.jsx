NewImageProgesss = React.createClass({
  getInitialState() {
    return {
      image1 : false,
      image2 : false,
      image3 : false
    }
  },
  render() {
    return (
      <div className="new-image-progress">
        <h3>Upload your current self!</h3>
        <p>
          You can add 3 images pro measurement. And Maximum 200 images pro account.
          Add a comment to your image to describe what you think about your current
          situation.
        </p>
        <textarea ref="commentRef" rows="5" className="form-control" ></textarea>
        <div className="row">
          <div className="col-xs-4">
            {this.state.image1 ?
              <img className="img-fluid" id="image1" src={this.state.image1}></img>
              :
              <i className="fa fa-user fa-5 text-xs-center"></i>
            }

            <span className="btn btn-info btn-sm btn-file">
                change <input type="file" id="imageUpload1" className="file_bag" onChange={this.uploadImageToProgress.bind(this,1)} accept="image/x-png, image/gif, image/jpeg" />
            </span>
          </div>
          <div className="col-xs-4">
            {this.state.image2 ?
              <img className="img-fluid" id="image2" src={this.state.image2}></img>
              :
              <i className="fa fa-user fa-5 text-xs-center"></i>
            }
            <span className="btn btn-info btn-sm btn-file">
                change <input type="file" id="imageUpload2" className="file_bag" onChange={this.uploadImageToProgress.bind(this,2)} accept="image/x-png, image/gif, image/jpeg" />
            </span>
          </div>
          <div className="col-xs-4">
            {this.state.image3 ?
              <img className="img-fluid" id="image3" src={this.state.image3}></img>
              :
              <i className="fa fa-user fa-5 text-xs-center"></i>
            }
            <span className="btn btn-info btn-sm btn-file">
                change <input type="file" id="imageUpload3" className="file_bag" onChange={this.uploadImageToProgress.bind(this,3)} accept="image/x-png, image/gif, image/jpeg" />
            </span>
          </div>
        </div>
        <button className="btn btn-success" onClick={this.createProgressRecord}>Save</button>
      </div>
    )
  },
  uploadImageToProgress(imgnumber,event) {
    let input = $(event.target)[0];
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = (e) => {
            switch (imgnumber) {
              case 2:
                this.setState({image2 : e.target.result});
                break;
              case 3:
                this.setState({image3 : e.target.result});
                break;
              default:
                this.setState({image1 : e.target.result});
                break;
            }
        }
        reader.readAsDataURL(input.files[0]);
    }
  },
  createProgressRecord() {
    const {commentRef} = this.refs;
    let files = [];
    this.state.image1 ? files.push($('#imageUpload1')[0].files) : null;
    this.state.image2 ? files.push($('#imageUpload2')[0].files) : null;
    this.state.image3 ? files.push($('#imageUpload3')[0].files) : null;
    const insertObj = {comment : commentRef.value};
    Meteor.call('insertStatusimages', insertObj, (err, _id) => {
      if(err){return err;}
      files.map((file,index) => {
        S3.upload({files:file,path:"statusimages/"+Meteor.userId()},(err,res) => {
          if(err){return err};
          Meteor.call('updateStatusimages',{url : res.url, key : res.relative_url, objId : _id})
        });
      })
      this.props.hideView();
    })

  }
})
