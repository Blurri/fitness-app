StatusimagesView = React.createClass({
  mixins: [EnsureAuthenticated, ReactMeteorData],
  getMeteorData() {
    let data = {};
    let statusId = this.props.id;
    let handle = Meteor.subscribe('singleStatusimage', statusId);
    if(handle.ready()) {
      data.statusimage = Statusimages.findOne({_id : statusId});
    }
    return data;
  },
  getContent() {
    const entry = this.data.statusimage;
    return (
      <div className="statusimage-view">
        <h1>{moment(entry.createdAt).fromNow()}</h1>
        <p>{entry.comment}</p>
        <div className="row">
          {entry.images.map((image,index)=>{
            return (
              <div className="col-xs-3" key={index}>
                <img src={image.url} className="img-fluid" />
              </div>
            )
          })}
        </div>
      </div>
    )
  },
  render() {
    return (
      <div>
        {this.data.statusimage ? this.getContent() : <p>Loading ...</p>}
      </div>
    )
  }
})
