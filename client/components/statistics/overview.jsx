StatisticOverview = React.createClass({
  mixins : [ReactMeteorData],
  getMeteorData() {
    let data = {};
    // let handle = Meteor.subscribe('getProgressRecords');
    // if(handle.ready()){
    //   data.progressrecords = ProgesssRecords.find({planId : this.props.planId}).fetch();
    // }
    // return data;
    //
    // var data = {};
    var planId = this.props.planId;
    var handle = Meteor.subscribe('singlePlan', planId);
    if(handle.ready()) {
      var plan = Plans.findOne({_id : planId})
      var handleExercises = Meteor.subscribe('getAllExerciseInList', plan.exercises);
      if(handleExercises.ready()){
        data.plan = plan;
        data.exercises = Exercises.find().fetch();
      }
    }
    return data;

  },
  getContent() {
    return (
      <div>
        <div className="row">
          <TypePie planId={this.props.planId} />
        </div>
        <div className="row" >
          {this.data.exercises.map((record, index)=> {
            return <StatExerciseCard exerciseId={record._id}  key={index} rowIndex={index}/>;
          })}
        </div>
      </div>
    )
  },
  render() {
    return (
      <div>
        {this.data.exercises ? this.getContent() : <p>Loading ...</p>}
      </div>
    )
  }
})
