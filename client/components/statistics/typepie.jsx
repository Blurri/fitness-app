TypePie = React.createClass({
  mixins : [ReactMeteorData],
  getMeteorData() {
    var data = {};
    var planId = this.props.planId;
    var handle = Meteor.subscribe('singlePlan', planId);
    if(handle.ready()) {
      var plan = Plans.findOne({_id : planId})
      var handleExercises = Meteor.subscribe('getAllExerciseInList', plan.exercises);
      if(handleExercises.ready()){
        data.plan = plan;
        data.exercises = Exercises.find().fetch();
      }
    }
    return data;
  },
  getContent() {
    return(
      <div id="piecart"></div>
    )
  },

  componentDidUpdate() {
    if(this.data.plan){
      const {plan,exercises} = this.data;
      let stammina = 0;
      let strenght = 0;
      let stretch = 0;

      exercises.map((rec, index) => {
          switch (rec.type) {
            case 'stammina':
            stammina += 1;
              break;
            case "strength":
            strenght += 1;
              break;
            default:
            stretch += 1;
          }
      })
      var data = {
        series: [{
          meta: 'stammina',
          value : stammina,
          name : 'Stammina'
        }, {
          meta: 'strength',
          value : strenght,
          name : 'Strength'
        }, {
          meta : 'stretch',
          value : stretch,
          name : 'Stretch'
        }]
        // series: [stammina,strenght,stretch],
      };
      let settings = {
      }
      var sum = function(a, b) { return a + b };
      new Chartist.Pie('#piecart', data,settings, {
        labelInterpolationFnc: function(value) {
          return Math.round(value / data.series.reduce(sum) * 100) + '%';
        }
      });
    }
  },
  render() {
    return (
      <div>
        {this.data.plan ? this.getContent() : <p>Loading ...</p>}
      </div>
    )
  }
})
