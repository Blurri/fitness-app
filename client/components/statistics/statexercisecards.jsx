StatExerciseCard = React.createClass({

  mixins : [ReactMeteorData],
  getMeteorData() {
    let data = {};
    const exerciseId = this.props.exerciseId;
    let handle = Meteor.subscribe('getProgressRecords');
    let handleExercise = Meteor.subscribe('singleExercise',exerciseId);
    if(handle.ready() && handleExercise.ready()){
      data.progressrecords = ProgesssRecords.find({exerciseId : exerciseId}).fetch();
      data.exercise = Exercises.findOne({_id : exerciseId})
    }
    return data;
  },
  getContent() {
    const {exercise, progressrecords} = this.data;
    return (
      <div className="card col-md-6">
        <h3>{exercise.title}</h3>
        <div id={'exercise'+this.props.rowIndex} className="ct-chart ct-golden-section"></div>
      </div>
    )
  },
  componentDidUpdate() {
    if(this.data.progressrecords && this.data.exercise){
      const {progressrecords,exercise} = this.data;

      let weightData = [{x: this.data.exercise.createdAt , y : (this.data.exercise.start_weight ? this.data.exercise.start_weight.value : 0 )}];
      let durationData = [{x: this.data.exercise.createdAt , y : (this.data.exercise.start_duration ? this.data.exercise.start_duration.value : 0)}];
      let distanceData = [{x: this.data.exercise.createdAt , y : (this.data.exercise.start_distance ? this.data.exercise.start_distance.value : 0)}];
      let numberOfSetsData = [{x: this.data.exercise.createdAt , y : (this.data.exercise.start_numberOfSets ? this.data.exercise.start_numberOfSets.value : 0)}];
      let repsInSetData = [{x: this.data.exercise.createdAt , y : (this.data.exercise.start_repsInSet ? this.data.exercise.start_repsInSet.value : 0)}];


      this.data.progressrecords.map((rec,index)=>{
        weightData.push({x: rec.createdAt, y : rec.newweight});
        durationData.push({x: rec.createdAt, y : rec.newduration});
        distanceData.push({x: rec.createdAt, y : rec.newdistance});
        numberOfSetsData.push({x: rec.createdAt, y : rec.newnumberOfSets});
        repsInSetData.push({x: rec.createdAt, y : rec.newrepsInSet});
      })

      let series = [{name: 'Weight',data: weightData},{name: 'Duration',data: durationData},{name: 'Distance',data: distanceData},{name: 'Number Of Sets',data: numberOfSetsData},{name: 'Reps in a set',data: repsInSetData}];

        new Chartist.Line('#'+ 'exercise'+this.props.rowIndex, {
          series: series
        }, {
          axisX: {
            type: Chartist.FixedScaleAxis,
            divisor: 5,
            labelInterpolationFnc: function(value) {
              return moment(value).format('MMM D');
            }
          },
          axisY: {
            onlyInteger: true,
            low: 0
          },
          series: {
            remaining: {
              lineSmooth: Chartist.Interpolation.step(),
              showPoint: false
            },
            stories: {
              lineSmooth: false
            }
          }
        });

    }
  },
  render() {
    return (
      <div>
        {this.data.progressrecords ? this.getContent() : <p>Loading ...</p>}
      </div>
    )
  }
})
