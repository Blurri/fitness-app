
EditProfileForm = React.createClass({
  render() {
    return (
      <div className="row">
        <div className="card card-block">
          <form>
            <fieldset className="form-group input-group-sm">
              <label>Birthday</label>
              <input type="date" className="form-control" defaultValue={this.props.defaultDate} onChange={this.props.dateDidChange} />
            </fieldset>
            <fieldset className="form-group input-group-sm">
              <label>Gender</label>
              <select  onChange={this.props.genderDidChange} defaultValue={this.props.gender} className="form-control" >
                <option value="select-one">please select</option>
                <option value="male" >Male</option>
                <option value="female" >Female</option>
              </select>
            </fieldset>
          </form>
        </div>
      </div>
    )
  }
})
