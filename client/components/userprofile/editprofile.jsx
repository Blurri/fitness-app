EditProfile = React.createClass({
    mixins : [EnsureAuthenticated,ReactMeteorData],
    getInitialState() {
      return {
        showMeasurments : true,
        showNewStatusimages : false,
        showStatusimagesList : false
      }
    },
    getMeteorData() {
      let data = {};
      data.currentUser = Meteor.user();
      return data;
    },
    getContent() {
      const {currentUser} = this.data;
      const profile = currentUser.profile || {};
      let defaultDate = profile.birthday ? moment(currentUser.profile.birthday).format('YYYY-MM-DD') : moment().format('YYYY-MM-DD');

      return (
        <div className="edit-profile">
          <div className="row">
            <div className="col-xs-4">
              <EditProfileAvatar avatar={profile.avatar} didSelectImage={this.didSelectImage} email={currentUser.emails[0].address} username={currentUser.username} />
              <EditProfileForm genderDidChange={this.genderDidChange} dateDidChange={this.dateDidChange} defaultDate={defaultDate} />
            </div>
            <div className="col-xs-8">
              <div className="row">
                <div className="col-xs-12">
                  <div className="btn-group btn-group-sm" role="group">
                    <button onClick={this.toggleShowStatusimagesList} className="btn-sm btn btn-info">Gallery</button>
                    <button onClick={this.showAddImages} className="btn-sm btn btn-info">Add Images</button>
                    <button onClick={this.toggleShowMeasurments} className="btn-sm btn btn-info">Measurement</button>
                  </div>
                </div>
              </div>
              <hr/>
              <div className="row">
                {this.state.showMeasurments ?
                  <div className="col-xs-12">
                    <NewMeasurement />
                    <hr/>
                    <Measurement showlatest={true} />
                  </div>
                  :
                  null
                }
                {this.state.showNewStatusimages ?
                  <div className="col-xs-12">
                    <NewImageProgesss hideView={this.toggleShowMeasurments} />
                  </div>
                  :
                  null
                }
                {this.state.showStatusimagesList ?
                  <div className="col-xs-12">
                    <div className="row">
                      <div className="col-xs-12">
                          <StatusimagesList />
                      </div>
                    </div>

                  </div>
                  :
                  null
                }
              </div>
            </div>
          </div>
        </div>
      )
    },
    render() {
      return (
        <div>
          {this.data.currentUser ? this.getContent():<p>Loading ...</p>}
        </div>
      )
    },
    showAddImages(event) {
      event.preventDefault();
      this.setState({
        showMeasurments : false,
        showNewStatusimages : true,
        showStatusimagesList : false
      });
    },
    toggleShowMeasurments() {
      this.setState({
        showMeasurments : true,
        showNewStatusimages : false,
        showStatusimagesList : false
      })
    },
    toggleShowStatusimagesList() {
      this.setState({
        showMeasurments : false,
        showNewStatusimages : false,
        showStatusimagesList : true
      })
    },
    didSelectImage(event) {
      const user = this.data.currentUser;
      const profile = user.profile || {};
      let files = $(event.target)[0].files;
      Meteor.call('deleteAvatarFromS3');
      S3.upload({files:files,path:"useravatar"},function(err,res){
        if(err){return err};
        Meteor.call('setUserprofileAvatar',{url: res.url, key: res.relative_url});
      });
    },
    dateDidChange(event) {
      Meteor.call('setProfileDate', {date : new Date(event.target.value)});
    },
    genderDidChange(event) {
      Meteor.call('setGender',{gender : event.target.value})
    }
})
