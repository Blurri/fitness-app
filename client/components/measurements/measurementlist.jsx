MeasurementList = React.createClass({
  mixins : [EnsureAuthenticated,ReactMeteorData],
  getMeteorData() {
    let data = {};
    let handle = Meteor.subscribe('allMeasurements');
    if(handle.ready()){
      data.measurements = Measurements.find().fetch();
    }
    return data;
  },
  getContent() {
    return (
      <div createClass="measurement-list">
        <h1>Measurement Hirtorie</h1>

          <div className="row">
            <div className="col-md-12">
              <table className="table table-sm">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>When</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  {this.data.measurements.map((measurement, index) => (
                    <tr key={measurement._id}>
                      <td>{index+1}</td>
                      <td>{moment(measurement.createdAt).fromNow()}</td>
                      <td>
                        <a href={'/edit-measurement/'+measurement._id}>Edit</a> |
                        <a href={'/measurement/'+measurement._id}>View</a>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
      </div>
    )
  },
  render() {
    return (
      <div>
        {this.data.measurements ? this.getContent():<p>Loading ...</p>}
      </div>
    )
  }
})
