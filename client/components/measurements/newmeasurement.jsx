NewMeasurement = React.createClass({
  mixins : [EnsureAuthenticated],
  render() {
    return (
      <div className="new-measurement">
        <h1>New Measurement</h1>
        <hr/>
        <div className="row">
          <div className="col-xs-12">
            <h4>Girth</h4>
            <div className="row">
              <div className="col-xs-3">
                <div className="form-group">
                  <label>Upper arm / cm</label>
                  <input type="number" className="form-control" ref="upperarmRef" placeholder="upper arm" />
                </div>
              </div>
              <div className="col-xs-3">
                <div className="form-group">
                  <label>Chest / cm</label>
                  <input type="number" className="form-control" ref="chestRef" placeholder="chest" />
                </div>
              </div>
              <div className="col-xs-3">
                <div className="form-group">
                  <label>Waist / cm</label>
                  <input type="number" className="form-control" ref="waistRef" placeholder="waist" />
                </div>
              </div>
              <div className="col-xs-3">
                <div className="form-group">
                  <label>Thigh / cm</label>
                  <input type="number" className="form-control" ref="thighRef" placeholder="thigh" />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-xs-3">
            <h4>Weight</h4>
              <div className="form-group">
                <label>Weight / kg</label>
                <input type="number" className="form-control" ref="weightRef" placeholder="weight" />
              </div>
          </div>
        </div>
        <div className="row">
          <div className="col-xs-12">
            <div className="btn-group pull-xs-left">
              <button onClick={this.saveMeasurement} className="btn btn-success">
                Save
              </button>
            </div>
          </div>
        </div>
      </div>
    )
  },
  saveMeasurement(event) {
    event.preventDefault();
    const {upperarmRef,chestRef,waistRef,thighRef,weightRef} = this.refs;
    const insertObj = { upperarm : upperarmRef.value, chest : chestRef.value, waist : waistRef.value, thigh : thighRef.value, weight : weightRef.value }
    Meteor.call('newmeasurement', insertObj, (err,res) => {
      if(err){
        //handle error
      }
      this.clearRefs();
    })
  },
  clearRefs(){
    const {upperarmRef,chestRef,waistRef,thighRef,weightRef} = this.refs;
    upperarmRef.value = null;
    chestRef.value = null;
    waistRef.value = null;
    thighRef.value = null;
    weightRef.value = null;
  }
})
