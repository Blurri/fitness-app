EditMeasurement = React.createClass({
  mixins : [EnsureAuthenticated,ReactMeteorData],
  getMeteorData() {
    let data = {};
    let measurementId = this.props.measurementId;
    let handle = Meteor.subscribe('singleMeasurement', measurementId);
    if(handle.ready()){
      data.measurement = Measurements.findOne({_id:measurementId});
    }
    return data;
  },
  getContent() {
    const {measurement} = this.data;
    return (
      <div className="edit-measurement" >
          <h1>Edit Measurement</h1>
          <hr/>
          <div className="row">
            <div className="col-xs-12">
              <h4>Girth</h4>
              <div className="row">
                <div className="col-xs-3">
                  <div className="form-group">
                    <label>Upper arm / cm</label>
                    <input type="number" className="form-control" defaultValue={measurement.upperarm} ref="upperarmRef" placeholder="upper arm" />
                  </div>
                </div>
                <div className="col-xs-3">
                  <div className="form-group">
                    <label>Chest / cm</label>
                    <input type="number" className="form-control" defaultValue={measurement.chest} ref="chestRef" placeholder="chest" />
                  </div>
                </div>
                <div className="col-xs-3">
                  <div className="form-group">
                    <label>Waist / cm</label>
                    <input type="number" className="form-control" defaultValue={measurement.waist} ref="waistRef" placeholder="waist" />
                  </div>
                </div>
                <div className="col-xs-3">
                  <div className="form-group">
                    <label>Thigh / cm</label>
                    <input type="number" className="form-control" defaultValue={measurement.thigh} ref="thighRef" placeholder="thigh" />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-xs-3">
              <h4>Weight</h4>
                <div className="form-group">
                  <label>Weight / kg</label>
                  <input type="number" className="form-control" defaultValue={measurement.weight} ref="weightRef" placeholder="weight" />
                </div>
            </div>
          </div>
          <div className="row">
            <div className="col-xs-12">
              <div className="btn-group pull-xs-left">
                <button onClick={this.updateMeasurement} className="btn btn-info">
                  Update
                </button>
              </div>
            </div>
          </div>
      </div>
    )
  },
  render() {
    return (
      <div>
        {this.data.measurement ? this.getContent() : <p>Loading ...</p>}
      </div>
    )
  },
  updateMeasurement(event){
    event.preventDefault();
    const {measurement} = this.data;
    const {upperarmRef,chestRef,waistRef,thighRef,weightRef} = this.refs;
    const updateObj = { upperarm : upperarmRef.value, chest : chestRef.value, waist : waistRef.value, thigh : thighRef.value,weight : weightRef.value, measurementId: measurement._id}
    Meteor.call('editmeasurement', updateObj,(err,res)=> {
      if(err){
        //HANDLE ERROR
      }
      FlowRouter.go('/userprofile')
    })
  }
})
