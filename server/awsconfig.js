
S3.config = {
    key: Meteor.settings.AWSAccessKeyId,
    secret: Meteor.settings.AWSSecretAccessKey,
    bucket: 'gabor.fitness'
};


Slingshot.createDirective("mobileStatusImageUpload", Slingshot.S3Storage, {
  bucket: "gabor.fitness",
  acl: "public-read",
  authorize: function () {
    return true;
  },
  key: function (file) {
    return "statusimages/"+Meteor.userId() + '/' + file.name;
  }
});

Slingshot.createDirective("mobileProfileImageUpload", Slingshot.S3Storage, {
  bucket: "gabor.fitness",
  acl: "public-read",
  authorize: function () {
    return true;
  },
  key: function (file) {
    return "profileimage/"+Meteor.userId() + '/' + file.name;
  }
});
