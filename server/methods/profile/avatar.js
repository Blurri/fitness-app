let AWS = Npm.require('aws-sdk');
Meteor.methods({
  'deleteAvatarFromS3'(){
    const user = Meteor.user();
    if(user.profile){
      let key = user.profile.key;
      if(key){
        let s3 = new AWS.S3({
          accessKeyId: Meteor.settings.AWSAccessKeyId,
          secretAccessKey: Meteor.settings.AWSSecretAccessKey
        });
        if(key.substring(0,1) == '/'){
          key = key.substring(1);
        }
        var params = {  Bucket: 'gabor.fitness', Key: key };
        s3.deleteObject(params, function(err, data) {
          if (err) console.log(err, err.stack);
          else     console.log(data,'deleted');
        });
      }
    }
  },
  'setUserprofileAvatar'({url, key}){
    new SimpleSchema({url: {type: 'String'}, key: {type: 'String'} })
    .validate({url, key});
    Meteor.users.update(Meteor.userId(), {$set: {'profile.avatar' : url, 'profile.key' : key}})
  }
})
