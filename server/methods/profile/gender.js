
Meteor.methods({
  'setGender'({gender}){
    const user = Meteor.user();
    new SimpleSchema({gender : {type: 'String'}}).validate({gender});
    Meteor.users.update(Meteor.userId(), {$set : {'profile.gender' : gender}});
  }
})
