Meteor.methods({
  'setProfileDate'({date}){
    const user = Meteor.user();
    new SimpleSchema({gender : {type: 'Date'}}).validate({date});
    Meteor.users.update(Meteor.userId(), {$set : {'profile.birthday' : date}});
  }
})
