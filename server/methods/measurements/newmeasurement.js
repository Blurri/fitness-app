let Future = Npm.require('fibers/future');
Meteor.methods({
  'newmeasurement'({upperarm,chest,waist,thigh,weight} ) {
    let future = new Future();
    new SimpleSchema({upperarm : {type: 'Number', optional: true},chest : {type: 'Number', optional: true},waist : {type: 'Number', optional: true},thigh : {type: 'Number', optional: true},weight : {type: 'Number', optional: true}})
    .validate({upperarm,chest,waist,thigh,weight});
    const insertObj = {upperarm,chest,waist,thigh,weight,userId: Meteor.userId(),createdAt : new Date()}
    Measurements.insert(insertObj,(err,_id)=>{
      if(err){future.return(err)};
      future.return(_id)
    })
    return future.wait();
  }
});
