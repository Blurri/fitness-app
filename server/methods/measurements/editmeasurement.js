let Future = Npm.require('fibers/future');

Meteor.methods({
  'editmeasurement'({ upperarm, chest, waist, thigh,weight, measurementId}) {
    let future = new Future();
    new SimpleSchema({upperarm : {type: 'Number', optional: true},chest : {type: 'Number', optional: true},waist : {type: 'Number', optional: true},thigh : {type: 'Number', optional: true},weight : {type: 'Number', optional: true}, measurementId : {type: 'Number'}})
    .validate({ upperarm, chest, waist, thigh,weight, measurementId});
    const insertObj = {upperarm,chest,waist,thigh,weight,updatedAt : new Date()}
    Measurements.update(measurementId,{$set : insertObj},(err,_id)=>{
      if(err){future.return(err)};
      future.return(_id)
    })
    return future.wait();
  }
});
