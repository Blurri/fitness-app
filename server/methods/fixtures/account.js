let Future = Npm.require('fibers/future');

Meteor.methods({
  'createTestAccount'(){
    let future = new Future();
    let testUser = Meteor.users.findOne({username : 'testuser'});
    if(testUser){
      Meteor.users.remove({_id : testUser._id}, (err) => {
        if(err){}
        Accounts.createUser({username: 'testuser', password: 'test123'})
        future.return();
      })
    }else {
      Accounts.createUser({username: 'testuser', password: 'test123'});
      future.return();
    }
    return future.wait();
  }
})
