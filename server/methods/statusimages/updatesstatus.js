Meteor.methods({
  'updateStatusimages'({url, key, objId}) {
    new SimpleSchema({
      url : {type : 'String'}, key : {type: 'String'}, objId: {type: 'String'}
    }).validate({url, key, objId});
    Statusimages.update(objId, {$addToSet : {images : {url : url, key : key}}});
  }
})
