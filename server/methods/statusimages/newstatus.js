let Future = Npm.require('fibers/future');

Meteor.methods({
  'insertStatusimages'({comment}) {
    let future = new Future();
    new SimpleSchema({
      comment : {type : 'String', optional : true}
    }).validate({comment});
    const insertObj = {comment , userId : Meteor.userId(), createdAt : new Date()};
    Statusimages.insert(insertObj, (err, _id) => {
      if(err){
        future.return(err);
      }
      future.return(_id);
    })
    return future.wait();
  }
})
