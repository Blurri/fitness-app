Meteor.publish('singleStatusimage',function (id) {
  return Statusimages.find({
    _id : id,
    userId: this.userId
  });
})

Meteor.publish('allStatusimages', function (){
  return Statusimages.find({
    userId: this.userId
  });
})
