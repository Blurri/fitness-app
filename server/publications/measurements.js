Meteor.publish('singleMeasurement',function (id) {
  return Measurements.find({
    _id : id,
    userId: this.userId
  });
})

Meteor.publish('latestMeasurement',function () {
  return Measurements.find({}, {sort: {createdAt:-1}});
})

Meteor.publish('allMeasurements',function () {
  return Measurements.find({userId: this.userId}, {sort : {createdAt : -1}});
})
