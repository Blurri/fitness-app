FlowRouter.route('/', {
  name: 'home',
  action: (params) => {
    ReactLayout.render(DefaultLayout, {
      content: <Home />
    })
    setTitle('Home')
  }
})



FlowRouter.route('/plans', {
  name: 'plans',
  action: (params) => {
    ReactLayout.render(DefaultLayout, {
      content: <PlanList />
    })
    setTitle('Plans');
  }
});


FlowRouter.route('/new-plan', {
  name : 'new plan',
  action: (params) => {
    ReactLayout.render(DefaultLayout, {
      content: <NewPlan />
    })
    setTitle('New Plan');
  }
})

FlowRouter.route('/edit-plan/:id', {
  name : 'edit plan',
  action: (params) => {
    ReactLayout.render(DefaultLayout, {
      content : <EditPlan planId={params.id} />
    })
    setTitle('Edit Plan');
  }
})



// Exercises
FlowRouter.route('/new-exercise', {
  name : 'new exercise',
  action: (params) => {
    ReactLayout.render(DefaultLayout, {
      content: <NewExercise />
    })
    setTitle('New Exercise');
  }
})

FlowRouter.route('/exercise/:id', {
  name : 'exercise',
  action: (params) => {
    ReactLayout.render(DefaultLayout, {
      content : <Exercise exerciseId={params.id} />
    })
    setTitle('Exercise');
  }
})

FlowRouter.route('/edit-exercise/:id', {
  name : 'Edit Exercise',
  action : (params) => {
    ReactLayout.render(DefaultLayout, {
      content: <EditExercise exerciseId={params.id} />
    })
    setTitle('Edit Exercise');
  }
})


FlowRouter.route('/exercises', {
  name : 'Exercises',
  action : (params) => {
    ReactLayout.render(DefaultLayout, {
      content : <ExerciseList />
    })
  }
})
// TRAINING

FlowRouter.route('/training/:id', {
  name : 'Training',
  action : (params) => {
    ReactLayout.render(DefaultLayout, {
      content : <Training trainingId={params.id} />
    })
    setTitle('Training');
  }
})


// Login

FlowRouter.route('/login', {
  name : 'login',
  action : (params) => {
    ReactLayout.render(DefaultLayout, {
      content : <AtFormReact />
    })
    setTitle('Login');
  }
})


// Userprofile

FlowRouter.route('/userprofile', {
  name : 'user-profile',
  action : (params) => {
    ReactLayout.render(DefaultLayout, {
      content : <EditProfile />
    })
    setTitle('User Profile');
  }
})

// Measurements

FlowRouter.route('/edit-measurement/:id', {
  name : 'edit measurement',
  action : (params) => {
    ReactLayout.render(DefaultLayout, {
      content : <EditMeasurement measurementId={params.id} />
    })
    setTitle('Edit Measurement');
  }
})

FlowRouter.route('/measurementlist', {
  name : 'measurement list',
  action : (params) => {
    ReactLayout.render(DefaultLayout, {
      content : <MeasurementList />
    })
    setTitle('Measurements');
  }
})


FlowRouter.route('/newimageprogress', {
  name : 'new image progess',
  action : (params) => {
    ReactLayout.render(DefaultLayout, {
      content : <NewImageProgesss />
    })
    setTitle('New Image Progress');
  }
})


FlowRouter.route('/statusimages/:id', {
  name : 'status image',
  action : (params) => {
    ReactLayout.render(DefaultLayout, {
      content : <StatusimagesView id={params.id} />
    })
    setTitle('Status Images');
  }
})




// STATISTICS

FlowRouter.route('/stats/:id', {
  name : 'Statistic Overview',
  action : (params) => {
    ReactLayout.render(DefaultLayout, {
      content : <StatisticOverview planId={params.id} />
    })
    setTitle('Statistic Overview  ');
  }
})

setTitle = (title) => {
  let base = 'Fitness';

  if (title) {
    return document.title = `${title} - ${base}`;
  }
  return document.title = base;
};
